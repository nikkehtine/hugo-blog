---
title: "About"
date: 2021-12-29T16:52:12+01:00
draft: false
---

## About me

- My name is Nikki *(not my legal name, just preferred)*
- I am a graphic design and web development student from Poland
- I also do some programming (in C and Rust, as well as Bash scripting) on the side
  - [Here are my git repos](https://gitlab.com/nikkehtine)
- I am familiar with software such as
  - **development:** git, git LFS, VS Code, vim
  - **multimedia:** Adobe Suite (Photoshop, Illustrator, InDesign), Figma
  - **Linux system administration:** Arch, Fedora, Ubuntu
- ...and writing in
  - HTML (proficient)
  - CSS (proficient)
  - JavaScript (beginner)
  - C (beginner)
  - Rust (beginner)
- I'm also a beginner gamedev, learning Godot and its own GDScript
- Most importantly, I'm a FOSS advocate and contributor, including donations to Mozilla Foundation and KDE Project
