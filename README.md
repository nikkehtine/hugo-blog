# [nikkeh-notes.netlify.app](https://nikkeh-notes.netlify.app/)

*Personal blog made with [Hugo](https://gohugo.io/)*

The purpose of this project is to get familiar with the Hugo framework. I do not plan on updating the website.
